package com.example.bfloranetvisualizer

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class DigitalStatusViewAdapter(
    var digital_vals : List<DataDigitalStatus>
): RecyclerView.Adapter<DigitalStatusViewAdapter.DigitalStatusViewHolder>() {

    inner class DigitalStatusViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val digitalValue: TextView
        val channelStatus: TextView
        val ledStatus: TextView

        init {
            // Define click listener for the ViewHolder's View.
            digitalValue = itemView.findViewById(R.id.textDigitalValue)
            channelStatus = itemView.findViewById(R.id.textChannelStatus)
            ledStatus = itemView.findViewById(R.id.textLedStatus)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DigitalStatusViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.single_item_digital_info, parent, false
        )
        return DigitalStatusViewHolder(view)
    }

    override fun getItemCount(): Int {
        return digital_vals.size
    }

    override fun onBindViewHolder(holder: DigitalStatusViewHolder, position: Int) {
        holder.digitalValue.text = digital_vals[position].digitalValue
        holder.channelStatus.text = digital_vals[position].channelStatus
        holder.ledStatus.text = digital_vals[position].ledStatus
    }
}