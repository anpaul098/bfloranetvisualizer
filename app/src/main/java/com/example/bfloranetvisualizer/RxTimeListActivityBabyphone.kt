package com.example.bfloranetvisualizer

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bfloranetvisualizer.basepackages.BaseSharedPreferences
import com.example.bfloranetvisualizer.bfdatamodel.ModelBabyphone

class RxTimeListActivityBabyphone : AppCompatActivity() {
    lateinit var rvRxTimeBabyphone : RecyclerView
    private lateinit var modelListBabyphone: List<ModelBabyphone>

    private fun getRxTimelList(): List<ModelBabyphone> {
        val sharedDb = BaseSharedPreferences(this)
        modelListBabyphone = sharedDb.getArrayById(
                R.string.shared_key_decoded_data_Babyphone,
                Array<ModelBabyphone>::class.java
        )
        return modelListBabyphone
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_received_time_list)

        getRxTimelList()
        rvRxTimeBabyphone = findViewById<RecyclerView>(R.id.rvRxTimes)
        val adapter = RxTimeListAdapterBabyphone(modelListBabyphone, R.layout.single_text_row)
        rvRxTimeBabyphone.setLayoutManager(LinearLayoutManager(this))
        rvRxTimeBabyphone.setAdapter(adapter)
    }
}