package com.example.bfloranetvisualizer

data class DataDigitalStatus(
    val digitalValue : String,
    val channelStatus : String,
    val ledStatus: String
)