package com.example.bfloranetvisualizer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class CustomerSelectActivity : AppCompatActivity() {
    lateinit var rvCustomers : RecyclerView
    private val customerList = listOf(
            CustomerList(R.drawable.bitfabrik_logo, "babyphone"),
            CustomerList(R.drawable.fes_logo, "fes"),
            CustomerList(R.drawable.seddin_logo, "michendorfseddingolf")
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_select)

        rvCustomers = findViewById<RecyclerView>(R.id.rvCustomerList)
        val adapter = CustomerListAdapter(customerList, R.layout.single_customer_select_row)
        rvCustomers.setLayoutManager(LinearLayoutManager(this))
        rvCustomers.setAdapter(adapter)
    }
}