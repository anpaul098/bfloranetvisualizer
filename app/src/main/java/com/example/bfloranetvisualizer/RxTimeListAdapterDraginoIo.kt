package com.example.bfloranetvisualizer

import android.content.Intent
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.example.bfloranetvisualizer.basepackages.BaseRecyclerViewAdapter
import com.example.bfloranetvisualizer.basepackages.BaseViewHolder
import com.example.bfloranetvisualizer.bfdatamodel.ModelDraginoIo
import com.google.gson.Gson


class RxTimeListAdapterDraginoIo(
        rxTimeLists : List<ModelDraginoIo>, layoutID:Int
): BaseRecyclerViewAdapter<ModelDraginoIo, RxTimeListViewHolderDraginoIo>(rxTimeLists,layoutID) {

    override fun getCustomHolder(view: View): RxTimeListViewHolderDraginoIo {
        return RxTimeListViewHolderDraginoIo(view)
    }
}

class RxTimeListViewHolderDraginoIo(itemView: View) : BaseViewHolder<ModelDraginoIo>(itemView){
    val singleRowText: TextView
    val singleRowLayout: LinearLayout
    private lateinit var model: ModelDraginoIo

    init {
        // Define click listener for the ViewHolder's View.

        singleRowText = itemView.findViewById(R.id.textSingleRow)
        singleRowLayout = itemView.findViewById(R.id.layoutSingleRow)
        singleRowLayout.setOnClickListener { goToDraginoIoView() }
    }

    override fun setValueToItem(model: ModelDraginoIo) {
        singleRowText.text = model.created_at
        this.model = model
    }

    private fun goToDraginoIoView(){
        val viewSelectIntent = Intent(itemView.context, IoSensorViewDragino ::class.java)
        viewSelectIntent.putExtra("modelDraginoIO", Gson().toJson(model))
        itemView.context.startActivity(viewSelectIntent)
    }

}