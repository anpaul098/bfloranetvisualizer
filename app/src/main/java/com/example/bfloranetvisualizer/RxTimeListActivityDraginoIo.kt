 package com.example.bfloranetvisualizer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bfloranetvisualizer.basepackages.BaseSharedPreferences
import com.example.bfloranetvisualizer.bfdatamodel.ModelDraginoIo

class RxTimeListActivityDraginoIo : AppCompatActivity() {
    lateinit var rvRxTimeDraginoIo : RecyclerView
    private lateinit var modelListDraginoIo: List<ModelDraginoIo>

    private fun getRxTimelList(): List<ModelDraginoIo> {
        val sharedDb = BaseSharedPreferences(this)
        modelListDraginoIo = sharedDb.getArrayById(
                R.string.shared_key_decoded_data_DraginoIo,
                Array<ModelDraginoIo>::class.java
        )
        return modelListDraginoIo
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_received_time_list)

        getRxTimelList()
        rvRxTimeDraginoIo = findViewById<RecyclerView>(R.id.rvRxTimes)
        val adapter = RxTimeListAdapterDraginoIo(modelListDraginoIo, R.layout.single_text_row)
        rvRxTimeDraginoIo.setLayoutManager(LinearLayoutManager(this))
        rvRxTimeDraginoIo.setAdapter(adapter)
    }
}