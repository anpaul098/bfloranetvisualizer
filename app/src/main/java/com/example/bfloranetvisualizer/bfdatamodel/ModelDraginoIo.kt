package com.example.bfloranetvisualizer.bfdatamodel

data class ModelDraginoIo(
        val id: Int,
        val deveui: String,
        val unix_time: String,
        val client_id: String,
        val created_at: String,
        val decoded_data: DecodedDataDraginoIO
)

data class DecodedDataDraginoIO(
        val gws_data: GwsDataDraginoIO,
        val sensor_data: SensorDataDraginoIO
)

data class GwsDataDraginoIO(
        val EUI: String,
        val ack: Boolean,
        val bat: Int,
        val cmd: String,
        val `data`: String,
        val dr: String,
        val fcnt: Int,
        val freq: Int,
        val gws: List<GwDraginoIO>,
        val port: Int,
        val seqno: Int,
        val toa: Int,
        val ts: Long
)

data class GwDraginoIO(
        val ant: Int,
        val gweui: String,
        val lat: Double,
        val lon: Double,
        val rssi: Int,
        val snr: Double,
        val time: String,
        val tmms: Int,
        val ts: Long
)

data class SensorDataDraginoIO(
        val Messagetype: String,
        val analog_current_in1: AnalogUnitMeasurement,
        val analog_current_in2: AnalogUnitMeasurement,
        val analog_voltage_in1: AnalogUnitMeasurement,
        val analog_voltage_in2: AnalogUnitMeasurement,
        val digital_info: DigitalInfo
)

data class AnalogUnitMeasurement(
        val u: String,
        val v: String
)

data class DigitalInfo(
        val digital_in1: DigitalUnitMeasurement,
        val digital_in2: DigitalUnitMeasurement,
        val digital_in3: DigitalUnitMeasurement,
        val digital_out1: DigitalUnitMeasurement,
        val digital_out2: DigitalUnitMeasurement,
        val digital_out3: DigitalUnitMeasurement,
        val relay_out1: RelayUnitMeasurement,
        val relay_out2: RelayUnitMeasurement
)

data class DigitalUnitMeasurement(
        val input: String,
        val led: String
)

data class RelayUnitMeasurement(
        val channel: String,
        val led: String
)


