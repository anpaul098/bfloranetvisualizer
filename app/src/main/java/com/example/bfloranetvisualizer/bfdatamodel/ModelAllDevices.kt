package com.example.bfloranetvisualizer.bfdatamodel

data class ModelAllDevices(
    val custname: String,
    val customer_id: Int,
    val custtable: String,
    val decoderid: Int,
    val deviceuid: String
)