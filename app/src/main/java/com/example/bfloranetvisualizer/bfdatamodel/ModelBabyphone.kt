package com.example.bfloranetvisualizer.bfdatamodel

import android.net.http.SslCertificate

data class ModelBabyphone(
    val client_id: String,
    val created_at: String,
    val decoded_data: DecodedDataBabyphone,
    val deveui: String,
    val id: Int,
    val unix_time: String
)

data class DecodedDataBabyphone(
        val gws_data: GwsDataBabyphone,
        val sensor_data: SensorDataBabyphone
)

data class GwsDataBabyphone(
        val EUI: String,
        val ack: Boolean,
        val bat: Int,
        val cmd: String,
        val `data`: String,
        val dr: String,
        val fcnt: Int,
        val freq: Int,
        val gws: List<GwBabyphone>,
        val port: Int,
        val seqno: Int,
        val toa: Int,
        val ts: Long
)

data class GwBabyphone(
        val ant: Int,
        val gweui: String,
        val lat: Double,
        val lon: Double,
        val rssi: Int,
        val snr: Double,
        val time: String,
        val tmms: Int,
        val ts: Long
)

data class GPSQuality(
        val sat_no: String,
        val status: String
)

data class SensorDataBabyphone(
        val battery_level: String,
        val dl_counter: Int,
        val gps_quality: GPSQuality,
        val Latitude: String,
        val Longitude: String,
        val Messagetype: String,
        val RSSI: String,
        val SNR: String,
        val Temperature: String,
        val ul_counter: Int,
        val binary_status: String
)