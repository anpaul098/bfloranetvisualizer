package com.example.bfloranetvisualizer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }

    fun goToCustomerListPage(view:android.view.View){
        val intent = Intent(this, CustomerSelectActivity::class.java)
        startActivity(intent)
    }
}