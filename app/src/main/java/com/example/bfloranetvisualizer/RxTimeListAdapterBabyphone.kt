package com.example.bfloranetvisualizer

import android.content.Intent
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.example.bfloranetvisualizer.basepackages.BaseRecyclerViewAdapter
import com.example.bfloranetvisualizer.basepackages.BaseViewHolder
import com.example.bfloranetvisualizer.bfdatamodel.ModelBabyphone
import com.google.gson.Gson

class RxTimeListAdapterBabyphone(
        rxTimeLists : List<ModelBabyphone>, layoutID:Int
): BaseRecyclerViewAdapter<ModelBabyphone, RxTimeListViewHolderBabyphone>(rxTimeLists,layoutID) {

    override fun getCustomHolder(view: View): RxTimeListViewHolderBabyphone {
        return RxTimeListViewHolderBabyphone(view)
    }
}

class RxTimeListViewHolderBabyphone(itemView: View) : BaseViewHolder<ModelBabyphone>(itemView){
    val singleRowText: TextView
    val singleRowLayout: LinearLayout
    private lateinit var model: ModelBabyphone

    init {
        // Define click listener for the ViewHolder's View.

        singleRowText = itemView.findViewById(R.id.textSingleRow)
        singleRowLayout = itemView.findViewById(R.id.layoutSingleRow)
        singleRowLayout.setOnClickListener { goToBabyphoneView() }
    }

    override fun setValueToItem(model: ModelBabyphone) {
        singleRowText.text = model.created_at
        this.model = model
    }

    private fun goToBabyphoneView(){
        val viewSelectIntent = Intent(itemView.context, BabyphoneViewActivity ::class.java)
        viewSelectIntent.putExtra("modelBabyphone", Gson().toJson(model))
        itemView.context.startActivity(viewSelectIntent)
    }

}