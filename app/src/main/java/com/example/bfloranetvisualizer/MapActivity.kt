package com.example.bfloranetvisualizer

import android.annotation.SuppressLint
import android.os.Bundle
import android.webkit.HttpAuthHandler
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity


class MapActivity : AppCompatActivity() {
    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)


        val map1 = findViewById<WebView>(R.id.map1)
        map1.settings.javaScriptEnabled = true

        map1.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                if (url != null) {
                    view?.loadUrl(url)
                }
                return true
            }
            override fun onReceivedHttpAuthRequest(
                view: WebView?,
                handler: HttpAuthHandler,
                host: String?,
                realm: String?
            ) {
                handler.proceed("anMish", "ftz7;gjh#g4d")
            }
        }
        map1.loadUrl("https://api.bitfabrik.de/gps3/images/fes_combined/gps_marker.html")
        //map1.setHttpAuthUsernamePassword("https://api.bitfabrik.de","realm text","anMish","ftz7;gjh#g4d")
                         //map1.loadUrl("http://www.foo.com/index.html")
        //val html = "<iframe width=\"450\" height=\"260\" style=\"border: 1px solid #cccccc;\" src=\"https://api.bitfabrik.de/gps3/images/fes_combined/gps_marker.html\" ></iframe>"
    //map1.loadUrl("https://api.bitfabrik.de/gps3/images/fes_combined/gps_marker.html" ) //.loadData(html, "text/html", null);

    }
}