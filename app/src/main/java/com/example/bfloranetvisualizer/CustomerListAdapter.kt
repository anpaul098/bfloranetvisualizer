package com.example.bfloranetvisualizer

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.bfloranetvisualizer.basepackages.BaseRecyclerViewAdapter
import com.example.bfloranetvisualizer.basepackages.BaseViewHolder
import com.google.gson.Gson

data class CustomerList(
        val customerImg: Int,
        val customerName: String
)

class CustomerListAdapter(
    customers : List<CustomerList>, layoutID:Int
): BaseRecyclerViewAdapter<CustomerList, CustomerListViewHolder>(customers,layoutID) {

    override fun getCustomHolder(view: View): CustomerListViewHolder {
        return CustomerListViewHolder(view)
    }
}

class CustomerListViewHolder(itemView: View) : BaseViewHolder<CustomerList>(itemView){
    val customerName: TextView
    val customerImage: ImageView
    val customerRowLayout: LinearLayout
    private lateinit var model: CustomerList

    init {
        // Define click listener for the ViewHolder's View.
        customerName = itemView.findViewById(R.id.textCustomerName)
        customerImage = itemView.findViewById(R.id.imageCustomer)
        customerRowLayout = itemView.findViewById(R.id.layoutSingleCustomer)
        customerRowLayout.setOnClickListener { goToCustomerHomePage() }
    }

    override fun setValueToItem(model: CustomerList) {
        customerName.text = model.customerName
        customerImage.setImageResource(model.customerImg)
        this.model = model
    }

    private fun goToCustomerHomePage(){
        val viewSelectIntent = Intent(itemView.context, CustomerHomePageActivity ::class.java)
        viewSelectIntent.putExtra("modelCustomerList", Gson().toJson(model))
        itemView.context.startActivity(viewSelectIntent)
    }

}

