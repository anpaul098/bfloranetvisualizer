package com.example.bfloranetvisualizer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.example.bfloranetvisualizer.basepackages.BaseSharedPreferences
import com.example.bfloranetvisualizer.bfdatamodel.ModelAllDevices
import com.example.bfloranetvisualizer.bfdatamodel.ModelDraginoIo
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit

class CustomerHomePageActivity : AppCompatActivity() {
    private lateinit var model: CustomerList

    fun getDataWithIntent(){
        val jsonModel = intent.getStringExtra("modelCustomerList")
        model = Gson().fromJson(jsonModel, CustomerList::class.java)
    }

    private fun getDevicesByCustomerFromServer() {
        val baseUrl = "https://api.bitfabrik.de"
        val client = OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(BasicAuthInterceptor("Polo", "Jose"))
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

        val bfApi = retrofit.create(BfApiCall::class.java)

        GlobalScope.launch(Dispatchers.IO) {
            try {
                print(model.customerName)
                val response = bfApi.getDevicesByCustomer("sensorinfo", "json",
                    model.customerName).execute()
                if (response.isSuccessful) {
                    println("Success")
                    val gsonObj = Gson()
                    val results = mutableListOf<ModelAllDevices>()
                    val arrayResponse = response.body()?.get(1) as List<Any>
                    print(arrayResponse)
                    for (singleResponse in arrayResponse){
                        val dataAsKotlinObj = gsonObj.fromJson(
                                gsonObj.toJson(singleResponse),
                                ModelAllDevices::class.java
                        )
                        results.add(dataAsKotlinObj)
                    }
                    val sharedDbCustomerDevices = BaseSharedPreferences(this@CustomerHomePageActivity)
                    sharedDbCustomerDevices.saveById(R.string.shared_key_customer_devices, results)

                    val sharedData = sharedDbCustomerDevices.getArrayById(
                            R.string.shared_key_customer_devices,
                            Array<ModelAllDevices>::class.java
                    )
                    withContext(Dispatchers.Main) {
                        onReceivedApiResponse()
                    }
                    //print(sharedData[0].custname)
                } else {
                    println("hgvlkv.")
                }
            } catch (e: IOException) {
                println("Error")
                println(e.message)
            }
        }
    }

    private fun onReceivedApiResponse(){
        val intent = Intent(this, CustomerDevicesActivity::class.java)
        intent.putExtra("modelCustomerList", Gson().toJson(model))
        startActivity(intent)
    }

    fun goToCustomerDevices(view: View?){

        // Create progressBar dynamically...
        val progressBar = ProgressBar(this)
        val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        //layoutParams.setMargins(30, 30, 30, 30)
        progressBar.layoutParams = layoutParams
        //progressBar.isIndeterminate = true


        val mainQueryLayout = findViewById<LinearLayout>(R.id.layoutHomeContent)
        // Add ProgressBar to LinearLayout
        mainQueryLayout.addView(progressBar)

        //val visibility = if (progressBar.visibility == View.GONE) View.VISIBLE else View.GONE
        //progressBar.visibility = visibility
        progressBar.visibility = View.VISIBLE

        getDevicesByCustomerFromServer()

    }

    fun goToDecodedDataQuery(view: View?){
        val intent = Intent(this, DecodedDataQueryActivity::class.java)
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_home_page)

        getDataWithIntent()

        val imgCustomerHome = findViewById<ImageView>(R.id.imageCustomerHome)
        imgCustomerHome.setImageResource(model.customerImg)

        //val btnDeviceList = findViewById<Button>(R.id.btnAllDevsCustHome)
        //btnDeviceList.setOnClickListener { goToCustomerDevices() }
    }
}