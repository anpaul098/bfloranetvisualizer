package com.example.bfloranetvisualizer.basepackages

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.bfloranetvisualizer.R


abstract class BaseSpinnerAdapter<M, V: BaseViewHolder<M>>(
    var context: Context, var dataList: List<M>, var layoutId: Int
) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    abstract fun getCustomHolder(view: View):V

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val viewHolder: V
        if (convertView == null) {
            view = inflater.inflate(layoutId, parent, false)
            viewHolder = getCustomHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as V
        }
        getItem(position)?.let { viewHolder.setValueToItem(it) }

        return view
    }

    override fun getItem(position: Int): M? {
        if (position==0) return null
        return dataList[position-1]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataList.size+1
    }

    override fun isEnabled(position: Int) = position != 0

}
