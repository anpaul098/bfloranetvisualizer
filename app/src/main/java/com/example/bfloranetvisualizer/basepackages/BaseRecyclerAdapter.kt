package com.example.bfloranetvisualizer.basepackages

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView


abstract class BaseViewHolder<M>(view: View) : RecyclerView.ViewHolder(view) {
    private var _position = 0
    abstract fun setValueToItem(model:M)

    fun setElementPosition(position: Int){
    _position = position
    }

    fun getElementPosition(): Int{
        return _position
    }

}

abstract class BaseRecyclerViewAdapter<M,V: BaseViewHolder<M>>  :
    RecyclerView.Adapter<V> {

    private lateinit var dataSet:List<M>
    private  var layoutID:Int = 0

    constructor(dataSet: List<M>,layoutID:Int):super(){
        this.dataSet = dataSet
        this.layoutID = layoutID
    }

    abstract  fun getCustomHolder(view: View):V

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): V {

        val view = LayoutInflater.from(viewGroup.context)
            .inflate(this.layoutID, viewGroup, false)
        return getCustomHolder(view)
    }
    override fun getItemCount() = dataSet.size
    override fun onBindViewHolder(holder: V, position: Int) {
        holder.setElementPosition(position)
        holder.setValueToItem(dataSet[position])
    }
}