package com.example.bfloranetvisualizer.basepackages

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson

class BaseSharedPreferences(val context: Context) {

    private val PREF_NAME = "MainSharedDatabase"
    val sharedPref: SharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    val gson = Gson()

    public fun <M> saveById(keyId: Int, value: M){
        save(context.getString(keyId), value)
    }

    private fun <M> save(keyString: String, value: M){
        with (sharedPref.edit()){
            putString(keyString, gson.toJson(value))
            apply()
        }
    }

    public  fun <M> getArrayById(keyId: Int, type: Class<Array<M>>): List<M> {
        return getArray(context.getString(keyId), type)
    }

    private  fun <M> getArray(key: String, type: Class<Array<M>>): List<M> {
        val jsonValue = sharedPref.getString(key, "")
        val objValue = gson.fromJson(jsonValue, type).toList()
        return objValue
    }

    public  fun delete(key:String){
        with (sharedPref.edit()){
            remove(key)
            apply()
        }
    }

    protected  fun clearAll(){
        with (sharedPref.edit()){
            clear()
            apply()
        }
    }
}