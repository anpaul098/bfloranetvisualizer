package com.example.bfloranetvisualizer

import com.example.bfloranetvisualizer.bfdatamodel.ModelBabyphone
import com.example.bfloranetvisualizer.bfdatamodel.ModelDraginoIo

class RootParser {
    fun isModelDraginoIo(model: Any): ModelDraginoIo? {
        val modelDraginoIo = model as? ModelDraginoIo
        modelDraginoIo?.let { return it }
        return null
    }

    fun isModelBabybphone(model: Any): ModelBabyphone?{
        val modelBabyphone = model as? ModelBabyphone
        modelBabyphone?.let { return it }
        return null
    }

    fun parser(models: List<Any>){
        var containerDraginoIo = mutableListOf<ModelDraginoIo>()
        var containerBabyphone = mutableListOf<ModelBabyphone>()

        for(model in models){
            val modelDraginoIo = model as? ModelDraginoIo
            modelDraginoIo?.let { containerDraginoIo.add(it)
            }
            if (modelDraginoIo != null) continue

            val modelBabyphone = model as? ModelBabyphone
            modelBabyphone?.let { containerBabyphone.add(it) }
        }
        print(containerDraginoIo)
        print(containerBabyphone)

    }

}
