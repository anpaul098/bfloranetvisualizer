package com.example.bfloranetvisualizer

import android.content.Context
import android.view.View
import android.widget.TextView
import com.example.bfloranetvisualizer.basepackages.BaseSpinnerAdapter
import com.example.bfloranetvisualizer.basepackages.BaseViewHolder


data class DropDownRowText(
    val rowText : String
)

class DropDownSpinnerViewHolder(itemView: View): BaseViewHolder<DropDownRowText>(itemView){
    val rowText: TextView

    init {
        rowText = itemView.findViewById(R.id.textSingleRow)
    }

    override fun setValueToItem(model: DropDownRowText) {
        rowText.text = model.rowText
    }
}

class  DropDownSpinnerAdapter(
    context: Context, dataList: List<DropDownRowText>, layoutId: Int
): BaseSpinnerAdapter<DropDownRowText, DropDownSpinnerViewHolder>(
    context, dataList, layoutId){
    override fun getCustomHolder(view: View): DropDownSpinnerViewHolder {
        return DropDownSpinnerViewHolder(view)
    }

}