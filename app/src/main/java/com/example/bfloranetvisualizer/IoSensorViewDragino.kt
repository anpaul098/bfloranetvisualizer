package com.example.bfloranetvisualizer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bfloranetvisualizer.bfdatamodel.ModelDraginoIo
import com.google.gson.Gson

class IoSensorViewDragino : AppCompatActivity() {

    lateinit var rvDigitalStatus : RecyclerView

    private lateinit var bfApi: BfApiCall
    private lateinit var model: ModelDraginoIo

    fun getData(){
        val jsonModel = intent.getStringExtra("modelDraginoIO")
        model = Gson().fromJson(jsonModel, ModelDraginoIo::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_io_sensor_view_dragino)

        getData()

        val devEui = findViewById<TextView>(R.id.textValDeveuiDraginoIo)
        val rawMessge = findViewById<TextView>(R.id.textValRawMessageDraginoIo)
        val rxTime = findViewById<TextView>(R.id.textValRxTimeDraginoIo)
        val dataRate = findViewById<TextView>(R.id.textValDataRateDraginoIo)
        val gws = findViewById<TextView>(R.id.textValGwsDraginoIo)
        val rssi = findViewById<TextView>(R.id.textValRssiDraginoIo)
        val snr = findViewById<TextView>(R.id.textValSnrDraginoIo)

        val measAC1 = findViewById<TextView>(R.id.valueToSetAC1)
        val measAC2 = findViewById<TextView>(R.id.valueToSetAC2)
        val measAV1 = findViewById<TextView>(R.id.valueToSetAV1)
        val measAV2 = findViewById<TextView>(R.id.valueToSetAV2)

        devEui.text = model.deveui
        rawMessge.text = model.decoded_data.gws_data.data
        rxTime.text = model.created_at
        dataRate.text = model.decoded_data.gws_data.dr
        gws.text = model.decoded_data.gws_data.gws[0].gweui
        rssi.text = model.decoded_data.gws_data.gws[0].rssi.toString()
        snr.text = model.decoded_data.gws_data.gws[0].snr.toString()

        measAC1.text = (model.decoded_data.sensor_data.analog_current_in1.v + " " +
                model.decoded_data.sensor_data.analog_current_in1.u)
        measAC2.text = (model.decoded_data.sensor_data.analog_current_in2.v + " " +
                model.decoded_data.sensor_data.analog_current_in2.u)
        measAV1.text = (model.decoded_data.sensor_data.analog_voltage_in1.v + " " +
                model.decoded_data.sensor_data.analog_voltage_in1.u)
        measAV2.text = (model.decoded_data.sensor_data.analog_voltage_in2.v + " " +
                model.decoded_data.sensor_data.analog_voltage_in2.u)

        val digitalStatusList = mutableListOf(
                DataDigitalStatus(
                        "relay_out1",
                        "Channel: " + model.decoded_data.sensor_data.digital_info.relay_out1.channel,
                        "LED: " + model.decoded_data.sensor_data.digital_info.relay_out1.led
                ),
                DataDigitalStatus(
                        "relay_out2",
                        "Channel: " + model.decoded_data.sensor_data.digital_info.relay_out2.channel,
                        "LED: " + model.decoded_data.sensor_data.digital_info.relay_out2.led
                ),
                DataDigitalStatus(
                        "digital_in3",
                        "Input: " + model.decoded_data.sensor_data.digital_info.digital_in3.input,
                        "LED: " + model.decoded_data.sensor_data.digital_info.digital_in3.led
                ),
                DataDigitalStatus(
                        "digital_in2",
                        "Input: " + model.decoded_data.sensor_data.digital_info.digital_in2.input,
                        "LED: " + model.decoded_data.sensor_data.digital_info.digital_in2.led
                ),
                DataDigitalStatus(
                        "digital_in1",
                        "Input: " + model.decoded_data.sensor_data.digital_info.digital_in1.input,
                        "LED: " + model.decoded_data.sensor_data.digital_info.digital_in1.led
                ),
                DataDigitalStatus(
                        "digital_out3",
                        "Input: " + model.decoded_data.sensor_data.digital_info.digital_out3.input,
                        "LED: " + model.decoded_data.sensor_data.digital_info.digital_out3.led
                ),
                DataDigitalStatus(
                        "digital_out2",
                        "Input: " + model.decoded_data.sensor_data.digital_info.digital_out2.input,
                        "LED: " + model.decoded_data.sensor_data.digital_info.digital_out2.led
                ),
                DataDigitalStatus(
                        "digital_out1",
                        "Input: " + model.decoded_data.sensor_data.digital_info.digital_out1.input,
                        "LED: " + model.decoded_data.sensor_data.digital_info.digital_out1.led
                )
        )

        rvDigitalStatus = findViewById<RecyclerView>(R.id.rvDigitalStatus)
        val adapter = DigitalStatusViewAdapter(digitalStatusList)
        rvDigitalStatus.setLayoutManager(LinearLayoutManager(this))
        rvDigitalStatus.setAdapter(adapter)

    }

}

