package com.example.bfloranetvisualizer

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface  BfApiCall {
    
    @GET("/iotconcepts/decodeddatabydate/") //?action=data&format=json&customer=fes&startdate=20201228T12:00:00&enddate=20201228T23:00:00&sensorid=A84041000181C90F")
    fun getDecodedDataByDate(@Query("action") action: String,
                             @Query("format") format: String,
                             @Query("customer") customer: String,
                             @Query("startdate") startdate: String,
                             @Query("enddate") enddate: String,
                             @Query("sensorid") sensorid: String):Call<List<Any>>

    @GET("/iotconcepts/decodeddatabydate/")
    fun getAllDevices(@Query("action") action: String,
                      @Query("format") format: String):Call<List<Any>>

    @GET("/iotconcepts/decodeddatabydate/")
    fun getDevicesByCustomer(@Query("action") action: String,
                             @Query("format") format: String,
                             @Query("customer") customer: String):Call<List<Any>>

}

