package com.example.bfloranetvisualizer

import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.bfloranetvisualizer.basepackages.BaseSharedPreferences
import com.example.bfloranetvisualizer.bfdatamodel.ModelBabyphone
import com.example.bfloranetvisualizer.bfdatamodel.ModelDraginoIo
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit


class QueryDecodedDataByDate{
    var action: String = ""
    var format: String = ""
    var customer: String = ""
    var startDate: String = ""
    var endDate: String = ""
    var sensorId: String = ""

    override fun toString(): String {
        return "\n action=$action\n format=$format\n customer=$customer\n start=$startDate\n end=$endDate\n id = $sensorId"
    }
}

class BasicAuthInterceptor(username: String, password: String): Interceptor {
    private var credentials: String = Credentials.basic(username, password)

    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        var request = chain.request()
        request = request.newBuilder().header("Authorization", credentials).build()
        return chain.proceed(request)
    }
}

class DecodedDataQueryActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    private val queryDecodedData = QueryDecodedDataByDate()
    private lateinit var context: Context

    private fun toDoubleDigit(value:Int): String {
        var stringValue = value.toString()
        if (value < 10) stringValue = "0$stringValue"
        return stringValue
    }

    private fun getDataFromServer() {
        val baseUrl = "https://api.bitfabrik.de"
        val client = OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(BasicAuthInterceptor("Polo", "Jose"))
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
        val bfApi = retrofit.create(BfApiCall::class.java)

        GlobalScope.launch(Dispatchers.IO) {
            try {
                val response = bfApi.getDecodedDataByDate(
                        queryDecodedData.action, queryDecodedData.format,
                        queryDecodedData.customer,
                        queryDecodedData.startDate, queryDecodedData.endDate,
                        queryDecodedData.sensorId).execute()
                if (response.isSuccessful) {
                    println("Success")
                    when (queryDecodedData.sensorId){
                        "A84041000181C90E" -> handleDraginoResponse(response)
                        "0018B200000211E5" -> handleBabyphoneResponse(response)
                    }
                } else {
                    println("hgvlkv.")
                }
            } catch (e: IOException) {
                println("Error")
                println(e.message)
            }
        }
    }

    private suspend fun handleDraginoResponse(response: Response<List<Any>>) {
        val gsonObj = Gson()
        val results = mutableListOf<ModelDraginoIo>()
        val arrayResponse = response.body()?.get(1) as List<Any>
        for (singleResponse in arrayResponse) {
            val dataAsKotlinObj = gsonObj.fromJson(
                    gsonObj.toJson(singleResponse),
                    ModelDraginoIo::class.java
            )
            results.add(dataAsKotlinObj)
        }

        val sharedDatabase = BaseSharedPreferences(this@DecodedDataQueryActivity)
        sharedDatabase.saveById(R.string.shared_key_decoded_data_DraginoIo, results)

        val sharedData = sharedDatabase.getArrayById(
                R.string.shared_key_decoded_data_DraginoIo,
                Array<ModelDraginoIo>::class.java
        )
        withContext(Dispatchers.Main) {
            //onReceivedApiResponse()
            val intent = Intent(context, RxTimeListActivityDraginoIo::class.java)
            startActivity(intent)
        }
        print(sharedData[0].client_id)
    }

    private suspend fun handleBabyphoneResponse(response: Response<List<Any>>) {
        val gsonObj = Gson()
        val results = mutableListOf<ModelBabyphone>()
        val arrayResponse = response.body()?.get(1) as List<Any>
        for (singleResponse in arrayResponse) {
            val dataAsKotlinObj = gsonObj.fromJson(
                    gsonObj.toJson(singleResponse),
                    ModelBabyphone::class.java
            )
            results.add(dataAsKotlinObj)
        }

        val sharedDatabase = BaseSharedPreferences(this@DecodedDataQueryActivity)
        sharedDatabase.saveById(R.string.shared_key_decoded_data_Babyphone, results)

        val sharedData = sharedDatabase.getArrayById(
                R.string.shared_key_decoded_data_Babyphone,
                Array<ModelDraginoIo>::class.java
        )
        withContext(Dispatchers.Main) {
            //onReceivedApiResponse()
            val intent = Intent(context, RxTimeListActivityBabyphone::class.java)
            startActivity(intent)
        }
        print(sharedData[0].client_id)
    }


    private fun onReceivedApiResponse(){
        val intent = Intent(this, RxTimeListActivityDraginoIo::class.java)
        startActivity(intent)
    }

    fun goToIoSensorViewPageDragino(view: View?){
        getDataFromServer()
        // Create progressBar dynamically...
        val progressBar = ProgressBar(this)
        progressBar.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        val mainQueryLayout = findViewById<LinearLayout>(R.id.layoutCalender)
        // Add ProgressBar to LinearLayout
        mainQueryLayout?.addView(progressBar)

        //val visibility = if (progressBar.visibility == View.GONE) View.VISIBLE else View.GONE
        //progressBar.visibility = visibility
        progressBar.visibility = View.VISIBLE
        }

    private val optionListAction = listOf(
            DropDownRowText("data"),
            DropDownRowText("info")
    )

    private val optionListFormat = listOf(
            DropDownRowText("json"),
            DropDownRowText("csv")
    )

    private val optionListCustomer = listOf(
            DropDownRowText("fes"),
            DropDownRowText("demo_customer"),
            DropDownRowText("seddin"),
            DropDownRowText("babyphone")
    )

    private val optionListDeveui = listOf(
            DropDownRowText("647FDA0000005D28"),
            DropDownRowText("A84041000181C90E"),
            DropDownRowText("A84041000181C90D"),
            DropDownRowText("0018B200000211E5")
    )

    //private val optionListDeveui = getDeviceDropDownList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_query_decoded_data)

        context = this

        val actionSpinner = findViewById<Spinner>(R.id.spinnerAction)
        actionSpinner.adapter = DropDownSpinnerAdapter(this, optionListAction,  R.layout.single_text_row)
        actionSpinner.onItemSelectedListener = this

        val formatSpinner = findViewById<Spinner>(R.id.spinnerFormat)
        formatSpinner.adapter = DropDownSpinnerAdapter(this, optionListFormat,  R.layout.single_text_row)
        formatSpinner.onItemSelectedListener = this

        val customerSpinner = findViewById<Spinner>(R.id.spinnerCustomer)
        customerSpinner.adapter = DropDownSpinnerAdapter(this, optionListCustomer,  R.layout.single_text_row)
        customerSpinner.onItemSelectedListener = this

        val deveuiSpinner = findViewById<Spinner>(R.id.spinnerDeveui)
        deveuiSpinner.adapter = DropDownSpinnerAdapter(this, optionListDeveui,  R.layout.single_text_row)
        deveuiSpinner.onItemSelectedListener = this

        val calenderStart: CalendarView = findViewById(R.id.calendarStartDate)
        calenderStart.setOnDateChangeListener { view, year, month, dayOfMonth ->
            queryDecodedData.startDate = "$year${toDoubleDigit(month+1)}${toDoubleDigit(dayOfMonth)}"

            val timePickerStart = TimePickerDialog(this,TimePickerDialog.OnTimeSetListener(
                    function = { view, hourOfDay, minute ->
                        queryDecodedData.startDate = queryDecodedData.startDate + "T" +
                                toDoubleDigit(hourOfDay)+":" + toDoubleDigit(minute)+":00"
                        print(queryDecodedData)
                    }
            ),0,0,true)

            timePickerStart.show()
        }

        val calenderEnd: CalendarView = findViewById(R.id.calendarEndDate)
        calenderEnd.setOnDateChangeListener { view, year, month, dayOfMonth ->
            queryDecodedData.endDate = "$year${toDoubleDigit(month+1)}${toDoubleDigit(dayOfMonth)}"

            val timePickerEnd = TimePickerDialog(this,TimePickerDialog.OnTimeSetListener(
                    function = { view, hourOfDay, minute ->
                        queryDecodedData.endDate = queryDecodedData.endDate + "T" +
                                toDoubleDigit(hourOfDay)+":"+toDoubleDigit(minute)+":00"
                    }
            ),0,0,true)

            timePickerEnd.show()
        }

    }
    override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
        // An item was selected. You can retrieve the selected item using
        // Below statement refers as "elvis" operator in Kotlin.
        // Replaces If with null.
        val item = parent.getItemAtPosition(pos) ?: return

        val selectedItem = item as DropDownRowText
        when (parent.id) {
            R.id.spinnerAction -> queryDecodedData.action = selectedItem.rowText
            R.id.spinnerFormat -> queryDecodedData.format = selectedItem.rowText
            R.id.spinnerCustomer -> queryDecodedData.customer = selectedItem.rowText
            R.id.spinnerDeveui -> queryDecodedData.sensorId = selectedItem.rowText
            else -> println("Invalid")
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // Another interface callback
        println("No non nonnn")
    }
}