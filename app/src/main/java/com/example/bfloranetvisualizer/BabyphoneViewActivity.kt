package com.example.bfloranetvisualizer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.bfloranetvisualizer.bfdatamodel.ModelBabyphone
import com.example.bfloranetvisualizer.bfdatamodel.ModelDraginoIo
import com.google.gson.Gson

class BabyphoneViewActivity : AppCompatActivity() {
    private lateinit var model: ModelBabyphone

    fun getData(){
        val jsonModel = intent.getStringExtra("modelBabyphone")
        model = Gson().fromJson(jsonModel, ModelBabyphone::class.java)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_babyphone_view)

        getData()

        val devEui = findViewById<TextView>(R.id.textValDeveuiBabyphone)
        val rawMessge = findViewById<TextView>(R.id.textValRawMessageBabyphone)
        val rxTime = findViewById<TextView>(R.id.textValRxTimeBabyphone)
        val dataRate = findViewById<TextView>(R.id.textValDataRateBabyphone)
        val gws = findViewById<TextView>(R.id.textValGwsBabyphone)
        val rssi = findViewById<TextView>(R.id.textValRssiBabyphone)
        val snr = findViewById<TextView>(R.id.textValSnrBabyphone)

        val ambTemp = findViewById<TextView>(R.id.valAmbTempBabyPhone)
        val latitude = findViewById<TextView>(R.id.valLatBabyphone)
        val longitude = findViewById<TextView>(R.id.valLongBabyphone)
        val gps = findViewById<TextView>(R.id.valGpsBabyPhone)
        val satelliteNo = findViewById<TextView>(R.id.valSatBabyphone)
        val ulCounter = findViewById<TextView>(R.id.valULCountBabyphone)
        val dlCounter = findViewById<TextView>(R.id.valDLCountBabyphone)

        devEui.text = model.deveui
        rawMessge.text = model.decoded_data.gws_data.data
        rxTime.text = model.created_at
        dataRate.text = model.decoded_data.gws_data.dr
        gws.text = model.decoded_data.gws_data.gws[0].gweui
        rssi.text = model.decoded_data.gws_data.gws[0].rssi.toString()
        snr.text = model.decoded_data.gws_data.gws[0].snr.toString()

        ambTemp.text = model.decoded_data.sensor_data.Temperature
        latitude.text = model.decoded_data.sensor_data.Latitude
        longitude.text = model.decoded_data.sensor_data.Longitude
        gps.text = model.decoded_data.sensor_data.gps_quality?.status
        satelliteNo.text = model.decoded_data.sensor_data.gps_quality?.sat_no?.toString()
        ulCounter.text = model.decoded_data.sensor_data.ul_counter.toString()
        dlCounter.text = model.decoded_data.sensor_data.dl_counter.toString()
    }

    fun goToBabyphoneMapView(view:android.view.View){
        val intent = Intent(this, MapActivity::class.java)
        startActivity(intent)
        //"https://api.bitfabrik.de/gps3/images/fes_combined/gps_marker.html"
    }
}