package com.example.bfloranetvisualizer

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bfloranetvisualizer.basepackages.BaseSharedPreferences
import com.example.bfloranetvisualizer.bfdatamodel.ModelAllDevices
import com.example.bfloranetvisualizer.bfdatamodel.ModelDraginoIo
import com.google.gson.Gson

class CustomerDevicesActivity : AppCompatActivity() {
    lateinit var rvCustomerDevices : RecyclerView
    private lateinit var customerDeviceList: List<ModelAllDevices>


    private fun getCustomerDevicesList(): List<ModelAllDevices> {
        val sharedDb = BaseSharedPreferences(this)
        customerDeviceList = sharedDb.getArrayById(
            R.string.shared_key_customer_devices,
            Array<ModelAllDevices>::class.java
        )
        return customerDeviceList
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_devices)


        getCustomerDevicesList()


        rvCustomerDevices = findViewById<RecyclerView>(R.id.rvCustomerDevices)
        val adapter = CustomerDeviceListAdapter(customerDeviceList, R.layout.single_customer_device_row)
        rvCustomerDevices.setLayoutManager(LinearLayoutManager(this))
        rvCustomerDevices.setAdapter(adapter)
    }
}