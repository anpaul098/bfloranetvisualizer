package com.example.bfloranetvisualizer

import android.content.Intent
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.example.bfloranetvisualizer.basepackages.BaseRecyclerViewAdapter
import com.example.bfloranetvisualizer.basepackages.BaseViewHolder
import com.example.bfloranetvisualizer.bfdatamodel.ModelAllDevices
import com.google.gson.Gson


class CustomerDeviceListAdapter(
    customers : List<ModelAllDevices>, layoutID:Int
): BaseRecyclerViewAdapter<ModelAllDevices, CustomerDeviceListViewHolder>(customers,layoutID) {

    override fun getCustomHolder(view: View): CustomerDeviceListViewHolder {
        return CustomerDeviceListViewHolder(view)
    }
}

class CustomerDeviceListViewHolder(itemView: View) : BaseViewHolder<ModelAllDevices>(itemView){
    val deviceEui: TextView
    val decoderId: TextView
    val appId: TextView
    val customerDeviceRowLayout: LinearLayout
    private lateinit var model: ModelAllDevices

    init {
        deviceEui = itemView.findViewById(R.id.textTitleDevEuis)
        decoderId = itemView.findViewById(R.id.textViewId)
        appId = itemView.findViewById(R.id.textAppId)
        customerDeviceRowLayout = itemView.findViewById(R.id.layoutSingleCustomerDevice)
        customerDeviceRowLayout.setOnClickListener { goToCustomerHomePage() }
    }

    override fun setValueToItem(model: ModelAllDevices) {
        deviceEui.text = model.deviceuid
        decoderId.text = model.decoderid.toString()
        appId.text = model.customer_id.toString()
        this.model = model
    }

    private fun goToCustomerHomePage(){
        val viewSelectIntent = Intent(itemView.context, CustomerHomePageActivity ::class.java)
        viewSelectIntent.putExtra("modelAllDevices", Gson().toJson(model))
        itemView.context.startActivity(viewSelectIntent)
    }

}