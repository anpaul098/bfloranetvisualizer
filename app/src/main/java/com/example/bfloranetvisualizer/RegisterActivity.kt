package com.example.bfloranetvisualizer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
    }

    fun goToAgriSensorViewPageTektelic(view:android.view.View){
        val intent = Intent(this, DecodedDataQueryActivity::class.java)
        startActivity(intent)
        //"https://api.bitfabrik.de/gps3/images/fes_combined/gps_marker.html"
    }
}